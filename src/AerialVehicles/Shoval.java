package AerialVehicles;


import Entities.Coordinates;

public class Shoval extends HaronFamily{
    String cameraType;

    public Shoval(int hoursFlew, String flightStatus, Coordinates motherBaseCoordinates,
                  int rocketsNumber, String rocketType, String sensorType, String cameraType) throws ObjectTypeNotExistException {
        super(hoursFlew, flightStatus, motherBaseCoordinates, rocketsNumber, rocketType, sensorType);
        this.cameraType = cameraType;
        objectTypeList = cameraTypeList;
        validateObjectType(cameraType);
    }

    @Override
    public String getAbility(String mission) {
        if (super.getAbility(mission) == null){
            return (cameraType);
        }
        return super.getAbility(mission);
    }
}

