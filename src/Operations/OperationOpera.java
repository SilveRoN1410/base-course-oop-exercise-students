package Operations;

import AerialVehicles.Eitan;
import AerialVehicles.F15;
import AerialVehicles.F16;
import AerialVehicles.ObjectTypeNotExistException;
import Entities.Coordinates;
import Missions.AerialVehicleNotCompatibleException;
import Missions.AttackMission;
import Missions.BdaMission;
import Missions.IntelligenceMission;

public class OperationOpera extends Operations{


    @Override
    public void executeOperation() throws AerialVehicleNotCompatibleException, ObjectTypeNotExistException {
        destination = new Coordinates(33.2033427805222, 44.5176910795946);
        homeBaseCoordinates = new Coordinates(31.827604665263365, 34.81739714569337);
        aerialVehicle = new Eitan(0,"ready", homeBaseCoordinates, 4,
                "Python", "InfraRed");
        mission = new IntelligenceMission(destination,
                "Dror Zalicha", aerialVehicle, "Iraq");
        System.out.println("Intelligence Mission: ");
        executeProcess(aerialVehicle, mission);
        aerialVehicle = new F15(0, "ready", homeBaseCoordinates, 4,
                "Spice250", "");
        mission = new AttackMission(destination, "Ze'ev Raz", aerialVehicle,
                "Tuwaitha Nuclear Research Center");
        System.out.println("Attack Mission: ");
        executeProcess(aerialVehicle, mission);
        aerialVehicle = new F16(0, "ready", homeBaseCoordinates,
                0, "", "Thermal");
        mission = new BdaMission(destination, "Ilan Ramon", aerialVehicle,
                "Tuwaitha Nuclear Research Center");
        System.out.println("Bda Mission: ");
        executeProcess(aerialVehicle, mission);
    }
}
