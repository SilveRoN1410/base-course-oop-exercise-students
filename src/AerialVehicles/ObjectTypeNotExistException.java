package AerialVehicles;

public class ObjectTypeNotExistException extends Exception{
    public ObjectTypeNotExistException(String message){
        super(message);
    }
}
