package AerialVehicles;

import Entities.Coordinates;

public abstract class Uav extends AerialVehicle{
    public Uav(int hoursFlew, String flightStatus, Coordinates motherBaseCoordinates) {
        super(hoursFlew, flightStatus, motherBaseCoordinates);
    }

    public void hoverOverLocation(Coordinates destination){
        flightStatus = "Flying in the air ";
        System.out.println("Hovering over: " + destination.getLongitude() + ", " + destination.getLatitude());
    }
}
