package AerialVehicles;

import Entities.Coordinates;

public abstract class HermesFamily extends Uav {
    String sensorType;
    String cameraType;

    public HermesFamily(int hoursFlew, String flightStatus, Coordinates motherBaseCoordinates,
                        String sensorType, String cameraType) throws ObjectTypeNotExistException {
        super(hoursFlew, flightStatus, motherBaseCoordinates);
        this.sensorType = sensorType;
        this.cameraType = cameraType;
        hoursWithoutRepair = 100;
        objectTypeList = sensorTypeList;
        validateObjectType(sensorType);
        objectTypeList = cameraTypeList;
        validateObjectType(cameraType);
    }

    @Override
    public String getAbility(String mission) {
        if(mission.equals("Bda")){
            return (cameraType);
        }else if(mission.equals("Intelligence")){
            return ("sensor type: " + sensorType);
        }else{
            return null;
        }
    }
}
