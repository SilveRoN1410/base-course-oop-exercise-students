package Missions;

import AerialVehicles.AerialVehicle;
import Entities.Coordinates;

public class AttackMission extends Mission{
    private final String target;


    public AttackMission(Coordinates missionDestinationCoordinates, String pilotName,
                         AerialVehicle operatingAerialVehicle, String target) throws AerialVehicleNotCompatibleException {
        super(missionDestinationCoordinates, pilotName, operatingAerialVehicle);
        this.target = target;
        capableAerialVehicles = new String[]{"F16", "F15", "Shoval", "Eitan", "Kochav"};
        validateCapability(operatingAerialVehicle);
    }

    @Override
    public void executeMission() {
        String mission = "Attack";
        System.out.println(pilotName+ ": " + operatingAerialVehicle.getClass().getSimpleName() + " Attacking " +
                target + " " + "with: " + operatingAerialVehicle.getAbility(mission));
    }


}
