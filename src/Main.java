import AerialVehicles.ObjectTypeNotExistException;
import Missions.AerialVehicleNotCompatibleException;
import Operations.OperationOpera;
import Operations.Operations;

public class Main {

    public static void main(String[] args) throws AerialVehicleNotCompatibleException, ObjectTypeNotExistException {
        Operations operation;
        operation = new OperationOpera();
        operation.executeOperation();

    }
}
