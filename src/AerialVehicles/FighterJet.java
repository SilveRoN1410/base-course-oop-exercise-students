package AerialVehicles;

import Entities.Coordinates;

public abstract class FighterJet extends AerialVehicle {
    int rocketsNumber;
    String rocketType;

    public FighterJet(int hoursFlew, String flightStatus, Coordinates motherBaseCoordinates,
                      int rocketsNumber, String rocketType) throws ObjectTypeNotExistException {
        super(hoursFlew, flightStatus, motherBaseCoordinates );
        this.rocketsNumber = rocketsNumber;
        this.rocketType = rocketType;
        hoursWithoutRepair = 250;
        objectTypeList = rocketTypeList;
        validateObjectType(rocketType);
    }

    @Override
    public String getAbility(String mission) {
        if (mission.equals("Attack")){
            return (rocketType + "X" + rocketsNumber);
        }
        return null;
    }
}
