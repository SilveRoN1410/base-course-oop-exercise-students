package Missions;

import AerialVehicles.AerialVehicle;
import Entities.Coordinates;

public abstract class Mission{
    Coordinates missionDestinationCoordinates;
    String pilotName;
    AerialVehicle operatingAerialVehicle;
    String[] capableAerialVehicles;

    public Mission(Coordinates missionDestinationCoordinates, String pilotName, AerialVehicle operatingAerialVehicle){
        this.missionDestinationCoordinates = missionDestinationCoordinates;
        this.pilotName = pilotName;
        this.operatingAerialVehicle = operatingAerialVehicle;
    }

    public void validateCapability(AerialVehicle operatingAerialVehicle) throws AerialVehicleNotCompatibleException {
        boolean valid = false;
        for( int i =0; i<=capableAerialVehicles.length - 1; i++){
            if (operatingAerialVehicle.getClass().getSimpleName().equals(capableAerialVehicles[i])) {
                valid = true;
                break;
            }
        }
        if(!valid){
            throw new AerialVehicleNotCompatibleException("The Aerial Vehicle you entered isn't compatible to do" +
                    " this mission, please enter another vehicle");
        }
    }

    public void begin()  {
        System.out.println("Beginning Mission! ");
        operatingAerialVehicle.flyTo(missionDestinationCoordinates);
    }

    public void cancel(){
        System.out.println("Abort Mission! ");
        operatingAerialVehicle.land(operatingAerialVehicle.getMotherBaseCoordinates());
    }

    public void finish(){
        operatingAerialVehicle.land(operatingAerialVehicle.getMotherBaseCoordinates());
        System.out.println("Finish Mission! ");
    }

    public void executeMission(){
    }


}
