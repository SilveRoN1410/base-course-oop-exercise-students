package AerialVehicles;

import Entities.Coordinates;

public class Eitan extends HaronFamily {


    public Eitan(int hoursFlew, String flightStatus, Coordinates motherBaseCoordinates,
                 int rocketsNumber, String rocketType, String sensorType) throws ObjectTypeNotExistException {
        super(hoursFlew, flightStatus, motherBaseCoordinates, rocketsNumber, rocketType, sensorType);
    }

    @Override
    public String getAbility(String mission) {
        return super.getAbility(mission);
    }
}
