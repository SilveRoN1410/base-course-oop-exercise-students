package Missions;

import AerialVehicles.AerialVehicle;
import Entities.Coordinates;

public class IntelligenceMission extends Mission{
    private final String region;

    public IntelligenceMission(Coordinates missionDestinationCoordinates, String pilotName,
                               AerialVehicle operatingAerialVehicle, String region) throws AerialVehicleNotCompatibleException {
        super(missionDestinationCoordinates, pilotName, operatingAerialVehicle);
        this.region = region;
        capableAerialVehicles = new String[]{"F15","Zik", "Shoval", "Eitan", "Kochav"};
        validateCapability(operatingAerialVehicle);
    }

    @Override
    public void executeMission() {
        String mission = "Intelligence";
        System.out.println(pilotName +": " + operatingAerialVehicle.getClass().getSimpleName() + " Collecting Data in " +
                region + " " + "with: " + operatingAerialVehicle.getAbility(mission));
    }
}
