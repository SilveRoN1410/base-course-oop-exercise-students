package AerialVehicles;

import Entities.Coordinates;

public abstract class AerialVehicle {
    protected int hoursWithoutRepair;
    int hoursFlew;
    String flightStatus;
    Coordinates motherBaseCoordinates;
    String[] objectTypeList;
    protected String[] sensorTypeList = new String[]{"InfraRed", "Elint", ""};
    protected String[] rocketTypeList = new String[]{"Python", "Amram", "Spice250", ""};
    protected String[] cameraTypeList = new String[]{"Regular", "Thermal", "NightVision", ""};

    public AerialVehicle(int hoursFlew, String flightStatus, Coordinates motherBaseCoordinates) {
        this.hoursFlew = hoursFlew;
        this.flightStatus = flightStatus;
        this.motherBaseCoordinates = motherBaseCoordinates;
    }

    public void validateObjectType(String objectType) throws ObjectTypeNotExistException {
        boolean valid = false;
        for( int i =0; i<=objectTypeList.length - 1 ; i++){
            if (objectType.equals(objectTypeList[i])) {
                valid = true;
                break;
            }
        }
        if(!valid){
            throw new ObjectTypeNotExistException("The object type you entered isn't exist" +
                    " please enter another type");
        }
    }

    public abstract String getAbility(String mission);

    public Coordinates getMotherBaseCoordinates() {
        return motherBaseCoordinates;
    }

    public void setFlightStatus(String flightStatus) {
        this.flightStatus = flightStatus;
    }

    public void flyTo(Coordinates destination){
        if (flightStatus.equals("ready")){
            System.out.println("Flying to " + destination.getLongitude() +", " + destination.getLatitude());
            setFlightStatus("Flying in the air");
        }  else if(flightStatus.equals("Flying in the air")){
            System.out.println("Aerial Vehicle already in air");
        }else{
            System.out.println("Aerial Vehicle isn't ready to fly");
        }
    }

    public void land(Coordinates destination){
        System.out.println("Landing on " + destination.getLongitude() + ", " + destination.getLatitude());
        check();
    }

    public void check(){
        if(hoursFlew < hoursWithoutRepair){
            setFlightStatus("ready");
        }else{
            setFlightStatus("not ready");
            repair();
        }
    }

    public void repair(){
        hoursFlew = 0;
        setFlightStatus("ready");
    }

    public String getFlightStatus() {
        return flightStatus;
    }
}
