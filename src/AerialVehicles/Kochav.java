package AerialVehicles;

import Entities.Coordinates;

public class Kochav extends HermesFamily{
    int rocketsNumber;
    String rocketType;

    public Kochav(int hoursFlew, String flightStatus, Coordinates motherBaseCoordinates,
                  String sensorType, String cameraType, int rocketsNumber, String rocketType) throws ObjectTypeNotExistException {
        super(hoursFlew, flightStatus, motherBaseCoordinates, sensorType, cameraType);
        this.rocketsNumber = rocketsNumber;
        this.rocketType = rocketType;
        objectTypeList = rocketTypeList;
        validateObjectType(rocketType);
    }

    @Override
    public String getAbility(String mission) {
        if (super.getAbility(mission) == null) {
            return (rocketType + "X" + rocketType);
        }
        return super.getAbility(mission);
    }
}
