package AerialVehicles;


import Entities.Coordinates;

public class F15 extends FighterJet{

    String sensorType;


    public F15(int hoursFlew, String flightStatus, Coordinates motherBaseCoordinates,
               int rocketsNumber, String rocketType, String sensorType) throws ObjectTypeNotExistException {
        super(hoursFlew, flightStatus, motherBaseCoordinates, rocketsNumber, rocketType);
        this.sensorType = sensorType;
        objectTypeList = sensorTypeList;
        validateObjectType(sensorType);
    }

    @Override
    public String getAbility(String mission) {
        if(super.getAbility(mission) == null){
            return ("sensor type: " + sensorType) ;
        }
        return super.getAbility(mission);
    }
}
