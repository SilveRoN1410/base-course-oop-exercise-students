package AerialVehicles;


import Entities.Coordinates;

public class F16 extends FighterJet{
    String cameraType;


    public F16(int hoursFlew, String flightStatus, Coordinates motherBaseCoordinates,
               int rocketsNumber, String rocketType, String cameraType) throws ObjectTypeNotExistException {
        super(hoursFlew, flightStatus, motherBaseCoordinates, rocketsNumber, rocketType);
        this.cameraType = cameraType;
        objectTypeList = cameraTypeList;
        validateObjectType(cameraType);
    }

    @Override
    public String getAbility(String mission) {
        if(super.getAbility(mission) == null){
            return (cameraType);
        }
        return super.getAbility(mission);
    }
}
