package Operations;

import AerialVehicles.AerialVehicle;
import AerialVehicles.ObjectTypeNotExistException;
import AerialVehicles.Uav;
import Entities.Coordinates;
import Missions.AerialVehicleNotCompatibleException;
import Missions.Mission;

public abstract class Operations {
    Coordinates destination;
    Coordinates homeBaseCoordinates;
    AerialVehicle aerialVehicle;
    Mission mission;

    public abstract void executeOperation() throws AerialVehicleNotCompatibleException, ObjectTypeNotExistException;

    public void executeProcess(AerialVehicle aerialVehicle, Mission mission){
        mission.begin();
        if(aerialVehicle instanceof Uav){
            ((Uav) aerialVehicle).hoverOverLocation(destination);
        }
        mission.executeMission();
        mission.finish();
    }
}
