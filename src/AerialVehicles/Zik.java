package AerialVehicles;


import Entities.Coordinates;

public class Zik extends HermesFamily{

    public Zik(int hoursFlew, String flightStatus, Coordinates motherBaseCoordinates,
               String sensorType, String cameraType) throws ObjectTypeNotExistException {
        super(hoursFlew, flightStatus, motherBaseCoordinates, sensorType, cameraType);
    }

    @Override
    public String getAbility(String mission) {
        return super.getAbility(mission);
    }
}