package Missions;

import AerialVehicles.AerialVehicle;
import Entities.Coordinates;

public class BdaMission extends Mission{
    private final String objective;

    public BdaMission(Coordinates missionDestinationCoordinates, String pilotName,
                      AerialVehicle operatingAerialVehicle, String objective) throws AerialVehicleNotCompatibleException {
        super(missionDestinationCoordinates, pilotName, operatingAerialVehicle);
        this.objective = objective;
        capableAerialVehicles = new String[]{"F16","Zik", "Shoval", "Kochav"};
        validateCapability(operatingAerialVehicle);
    }

    @Override
    public void executeMission() {
        String mission = "Bda";
        System.out.println(pilotName + ": " + operatingAerialVehicle.getClass().getSimpleName() + " taking pictures of " +
                objective + " " + "with: " + operatingAerialVehicle.getAbility(mission));
    }
}
