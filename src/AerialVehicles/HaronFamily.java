package AerialVehicles;

import Entities.Coordinates;

public abstract class HaronFamily extends Uav{
    int rocketsNumber;
    String rocketType;
    String sensorType;

    public HaronFamily(int hoursFlew, String flightStatus, Coordinates motherBaseCoordinates,
                       int rocketsNumber, String rocketType, String sensorType) throws ObjectTypeNotExistException {
        super(hoursFlew, flightStatus, motherBaseCoordinates);
        this.rocketsNumber = rocketsNumber;
        this.rocketType = rocketType;
        this.sensorType = sensorType;
        hoursWithoutRepair = 150;
        objectTypeList = rocketTypeList;
        validateObjectType(rocketType);
        objectTypeList = sensorTypeList;
        validateObjectType(sensorType);
    }

    @Override
    public String getAbility(String mission) {
        if(mission.equals("Attack")){
            return (rocketType + "X" + rocketType);
        }else if(mission.equals("Intelligence")){
            return ("sensor type: " + sensorType);
        }else{
            return null;
        }
    }
}
